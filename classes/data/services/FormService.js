class FormService {

    static shared(){
        if(!this.instance){
            this.instance = new this();
        }

        return this.instance;
    }

    getQuestions(){
        return new Promise(function (resolve) {
            const questions = [];
            form_feed.questions.forEach(question => {
                questions.push(new Question(question.id, question.question, question.answers.map(answer => new Answer(answer.id, answer.answer, answer.quantity))));
            });

            resolve(questions);
        });
    }

}