const form_feed = {
    questions: [
        {
            id: "question_0",
            question: "Dans votre quotidien avez vous des réflexes écologiques ?",
            answers: [
                {
                    id: "answer_0",
                    answer: "En permanence",
                    quantity: 0
                },
                {
                    id: "answer_1",
                    answer: "Souvent",
                    quantity: 1
                },
                {
                    id: "answer_2",
                    answer: "De temps en temps",
                    quantity: 2
                },
                {
                    id: "answer_3",
                    answer: "Rarement",
                    quantity: 3
                }
            ]
        }
    ]
};