class Question {

    constructor(id, question, answers) {
        this.id = id;
        this.question = question;
        this.answers = answers;
    }

}