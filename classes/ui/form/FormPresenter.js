class FormPresenter {

    constructor(viewController) {
        this.viewControlller = viewController;

        //data
        this.currentQuestion = 1; // On met à 1 mais l'indice du tableau commence à 0
        this.questions = [];

        //services
        this.formService = FormService.shared();

        this.getQuestions();
    }

    getQuestions(){
        this.formService.getQuestions().then(questions => {
           this.questions = questions;
           this.displayQuestion();
        });
    }

    displayQuestion(){
        const question = this.questions[this.currentQuestion - 1];
        this.viewControlller.setQuestionIndex(this.currentQuestion);
        this.viewControlller.setQuestion(question.question);
        this.viewControlller.setAnswers(question.answers);
    }

}