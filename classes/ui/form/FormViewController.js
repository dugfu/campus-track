class FormViewController {

    constructor() {
        this.ui = new FormPresenter(this);
    }

    setQuestionIndex(index){
        // noinspection JSUnresolvedFunction
        $(".card").find(".card-header").html('<div class="indice">'+index+'</div> Question '+index);
    }

    setQuestion(question){
        // noinspection JSUnresolvedFunction
        $(".card").find(".card-body").html('<p class="p-0 m-0 text-center">'+question+'</p>');
    }

    setAnswers(answers){
        answers.forEach((answer, index) => {
            index++;
            // noinspection JSUnresolvedFunction
            $("#answers-container").append('<p>'+index+') '+answer.answer+'</p>');
            // noinspection JSUnresolvedFunction
            $("#answer-buttons-container").append('<button class="text-primary border-0 text-uppercase">Choix '+index+'</button>');
        });
    }

}